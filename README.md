# Tikal Home Task

## Prerequisites Installation
- Please make sure you have 'Helm' installed on you machine. See https://helm.sh/docs/intro/install/ for insturctions.

## The Application
I've created a Python application that select and read up to 4 files from a local directory 'documents'.
The application will process the words appear in all the files and return the most 2 common words in those documents and how much they appear.

The application will run as a container inside a K8s cluster and can be triggered using GET request to '/process'.
When accessing the Endpoint, the application will select up to 4 files, process them and return the results.

The available files should be copied to 'documents' folder within the Git repository and will be build as part of the Docker image.

I created a custom 'Helm Chart' deploying the relevent deployments and services in order to the application to work.

Alongside the application, we will installed Prometheus and Grafana for monitoring.


# Cloning:
Clone the repository to local machine: 
- git clone https://gitlab.com/or.mishani/tikal.git"

# Add your documents:
My application will accept the possible documents up ahead, instead of mounting a volume later on. Therefore please copy all possible documents to 'documents' folder within the repository. 
I already have 5 files there if needed.
- cp -fr <local_machine_documents_directory_path>/* <git_repository_direcotry>/documents


# k3d (local cluster) instructions:
1. Make sure you are in the root folder of the repository.
2. Build the image to local using the following command (change the tag if needed): 
    - **docker build -t tikal:v1.0 .**
3. Create a K3d local cluster and wait for cluster to be up and running:
    - **k3d cluster create mycluster --port "80:80@loadbalancer"**
4. Create a new tikal namespace:
    - **kubectl create ns tikal**
5. Switch to new namespace:
    - **kubens tikal**
6. Import new created image to K3d (use the same tag as the build procees): 
    - **k3d image import tikal:v1.0 --cluster mycluster**
7. Enter 'helm' folder within the repository folder
8. Make sure 'value.yaml' contains the right image tag (default is 'v1.0', change it if needed).
9. Install 'Helm Chart' and wait for the pods & services to be ready:
    - **helm install tikal .**
10. Since K3d vanila doesn't support LoadBalancer by default, run the following command to expose the application:
    - **kubectl port-forward service/fastapi-service 8000:80**
11. Open a new second terminal and enter 'Prometheus' folder from the root folder of the repository.    
12. Install Prometheus latest tag with our custom 'values-prometheus.yaml' file using the following command: 
    - **helm install prometheus prometheus-community/prometheus --values values-prometheus.yaml**
13. Enter 'Grafana' folder from the root folder of the repository.
14. Install Grafana latest tag with out custom 'values-grafana.yaml' file using the following command: 
    - **helm install grafana grafana/grafana -f values-grafana.yaml**
15. Run the following command to expose grafana: 
    - **kubectl port-forward service/grafana 9000:80**
16. Using a browser, access the Grafana dashboards on **"http://localhost:9000"** with those credentials: **"username: admin, password: tikal"**.
17. In Grafana, on the left burger manu, select **'Connection -> Data Sources'**.
18. Press **'Add data source' and select 'Prometheus'**.
19. Under **'Connection -> Prometheus server URL** insert **http://prometheus-server** and press **'Save & Test'**.
20. In Grafana, on the left burger manu, select **Dashboards** and press **Create Dashboard** and **Add Visualization**.
21. Select Prometheus as data source and under **metric** select **total_request_total** to view all the requests. You can also add **selected_files** and **common_word_count** metrics. Don't forget to save the new added dashboard.
22. You can generate requests from your browser by accessing **"http://localhost:8000/process"** and view results in the Grafana dashboards.

# Managed K8s in Cloud Provider instructions:
In case you have a working cluster & Dockerhub account assosiated with,
we want to build our application into Docker image and push it to the repository (can also work with a differnet artifactory)

1. Make sure you are in the root folder of the repository.
2. Build the image to Dockerhub (or any other artifactory of your choice) by running the following command: 
    - **docker build --platform=linux/amd64 -t <your_articatory_path>/tikal:v1.0 .** 
    * In this case, we assume the cluster nodes are running amd64 architectory, but this can be changed depending on the artifetctory of your nodes.
3. Push the docker image to remote artifactory: 
    - **docker push <your_articatory_path>/tikal:v1.0**
4. Make sure your kubeconfig is in context with your cluster.
5. Create a new tikal namespace:
    - **kubectl create ns tikal**
6. Switch to new namespace:
    - **kubens tikal**
7. Enter 'helm' folder within the repository folder
8. Make sure 'value.yaml' contains the right image tag (default is 'v1.0', change it if needed).
9. Install 'Helm Chart' and wait for the pods & services to be ready:
    - **helm install tikal .**
10. Extract the 'External-IP' of the LoadBalancer for 'fastapi-service' by running 
    - **kubectl get svc**
11. Enter 'Prometheus' folder from the root folder of the repository.    
12. Install Prometheus latest tag with our custom 'values-prometheus.yaml' file using the following command: 
    - **helm install prometheus prometheus-community/prometheus --values values-prometheus.yaml**
13. Enter 'Grafana' folder from the root folder of the repository.
14. Install Grafana latest tag with out custom 'values-grafana.yaml' file using the following command: 
    - **helm install grafana grafana/grafana -f values-grafana.yaml**
15. Extract the 'External-IP' of the LoadBalancer' for 'grafana' but running again:
    - **kubectl get svc**
16. Using a browser, access the Grafana dashboards on **"http://|Grafana-External-IP|"** with those credentials: **"username: admin, password: tikal"**.
17. In Grafana, on the left burger manu, select **'Connection -> Data Sources'**.
18. Press **'Add data source' and select 'Prometheus'**.
19. Under **'Connection -> Prometheus server URL** insert **http://prometheus-server** and press **'Save & Test'**.
20. In Grafana, on the left burger manu, select **Dashboards** and press **Create Dashboard** and **Add Visualization**.
21. Select Prometheus as data source and under **metric** select **total_request_total** to view all the requests. You can also add **selected_files** and **common_word_count** metrics. Don't forget to save the new added dashboard.
22. You can generate requests from your browser by accessing **"http://|External-IP|/process"** and view results in the Grafana dashboards.

